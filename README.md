# README #

There are two classes for this assignment. One is rotate.java, and the other is rotateTest.java. The second class is to test the first class, using org.junit.Assert.* function. These two files is in src file, so please look into src file.

For this assignment, I didn't use in-place algorithm because I though it would be much easier to make a new Integer[] and update its value without messing the information in intArray that is passed into the method rotate.
Because of this decision, I need to loop through original array of integers only once, but it takes more space in memory.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact