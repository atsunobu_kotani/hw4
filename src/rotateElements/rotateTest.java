package rotateElements;
import static org.junit.Assert.*;
import java.util.*;
import rotateElements.rotate;
import org.junit.Test;

public class rotateTest {

	Integer[] intList = {1, 2, 3};
	Integer[] answer1 = {3, 1, 2};
	Integer[] answer2 = {2, 3, 1};
	Integer[] answer3 = {1, 2, 3};
	Integer[] intList2 = {1, 2, 3, 4, 5};
	Integer[] answer21 = {5, 1, 2, 3, 4};
	Integer[] answer22 = {4, 5, 1, 2, 3};
	
	@Test
	public void testRotation() {
		assertArrayEquals(answer1,rotate.rotate(intList, 1));
		assertArrayEquals(answer2,rotate.rotate(intList, 2));
		assertArrayEquals(answer3,rotate.rotate(intList, 3));
		assertArrayEquals(answer21, rotate.rotate(intList2, 1));
		assertArrayEquals(answer22, rotate.rotate(intList2, 2));
		assertArrayEquals(answer22, rotate.rotate(intList2, 7));
	}
}