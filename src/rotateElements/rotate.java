package rotateElements;

import java.util.Collection;
import java.util.Iterator;

public class rotate<E>{
	public static Integer[] rotate(Integer[] intArray, int rotationValue){
		int length = intArray.length;
		if(rotationValue % length == 0){
			return intArray;
		}
		Integer[] answerList = new Integer[length];
		int actualRotationValue = rotationValue % length;
		for(int i = 0; i < length; i++){
			if(i < length - actualRotationValue){
				answerList[actualRotationValue+i] = intArray[i];
			} else {
				answerList[i-(length-actualRotationValue)] = intArray[i];
			}
		}
		return answerList;
	}
}